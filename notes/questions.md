# General note

> translation are non-official, thus may contain slightly wrong statements or
> expressions. Please refer to the official (german) document, whenever possible


# Question 1

*Rough translation*:

Which federal ministries are currently using free software and which (Free)
licenses are in use exactly?

**Answer:**

- Data about 822 software pieces were given by the administrations
- We could identify 858 licenses, 625 of which are compatible with the GPL, 94
  are not and additional 139 are proprietary (and should not have been in the
  data in the first place)

**Notes**:

- Data is of extremely poor Quality, in many cases this question can not be
  answered by the data provided
- *Update: Due to a lot of manual work, a solution was found*

![Free licenses used in German administration](../plots/question-01_free-license-used.png)
\ 

* * *

# Question 2

*Rough translation*:

How high were IT-costs between 2013 and 2017 in the federal ministries and how
high were the cost savings in those that used Free as opposed to proprietary
software?

**Answer:**

- Costs rose dramatically overall (see plots for question2)
- Costs rose by several percents per year (see plots for question 2)
- numbers for cost saving are only given for 3 agencies. Total: 1M€ savings (no
  context given)

**Results of Analysis**:

- Question about cost savings through use of opensource software can not be
  answered sufficiently by the data.
- We were howerver able to create visualisations of the data provided. On the
  images below you can find:
  1. The overall moving average of development costs
  2. The average growth by agencies
- The agency BMAS is responsible for most of the increase.

![Overall moving average of development costs](../plots/question-2_costdevelopment-total.png)
\ 

![Cost development per agency](../plots/question-2_costdevelopment-peragency.png)
\ 

* * *

# Question 3

*Rough translation*:

Does the federal government plan to use free software in the federal
administration in the future as well? If so, where, why & when?

**Answer:**

> The possible use of Free Software is generally examined within the framework
> of the applicable guidelines. This is an ongoing process. However, the
> decisive factor for the selection of software is whether the required
> capabilities can be achieved in the overall system context. To this end,
> criteria such as functionality, interoperability, security, implementation,
> maintenance and training costs, the availability of specialist applications
> and usability must be checked. Where it makes sense and is economical, the use
> of Open Source products or Free Software is planned. For example, the Federal
> Office for the Protection of the Constitution (Bundesamt für
> Verfassungsschutz) is currently preparing to replace the manufacturer-bound HP
> Unix server with a Linux-based server infrastructure (a time horizon can not
> be estimated during this planning phase).

**ToDo:**

- It would be nice to give an official answer / statement to this by the FSFE.

* * *

# Question 4

*Rouch translation*:

What stance does the federal government have towards the standardisation of the
usage of free software in all federal agencies and to make it simpler to
acquire and use free software? Which concrete efforts does the federal
government take?

**Answer**:

> In principle, the Federal Government strives for the most standardized and
> thus uniform software product landscape possible. This is also one of the
> goals of the federal government's current IT consolidation. Basically, this
> also applies to the use of Free Software. With regard to procurement,
> reference is made to the answer to question 3.

**ToDo**:

- Does the FSFE agree with this approach?

* * *

# Question 5

*Rough translation*:

What stance does the federal government have towards the usage of Free licensing
of the product as binding condition in the commissioning of service providers?
Which concrete efforts does the federal government take?

**Answer**:

> As part of the Supplementary Terms and Conditions for the Procurement of
> Information Technology (EVB-IT), the Federal Government provides model
> contracts in which the rights of use can be regulated in detail when contracts
> are awarded (rights of use matrix). Whether and to what extent Free licenses
> are taken into account when placing an order depends on the respective
> requirements of the procurement order. For the rest, reference is made to
> answer to question 3.

**ToDo**:

- Does the FSFE agree with this approach?

* * *

# Question 6

*Rough Translation*:

What results do exist for the opensource projects which were supported based on
the guideline "Software Sprint"?

**Answer**:

> The "Software Sprint" funding measure, which is tailored entirely to
> developers of open source software projects, was launched by the German
> Federal Ministry of Education and Research (BMBF) in 2016. As a result of the
> first round of applications, 17 projects will be funded. Topics of the
> projects are citizen-oriented applications and competence in handling data.
> The second round of applications has just ended. The internal preliminary
> review of the sketches is underway.

**ToDo**:

- Statement by the FSFE?
- Research (see Notes below)

**Notes**:

- There was no additional data, regarding this question. Which projects were
  chosen? Why? How does the "funding" look like exactly?

* * *

# Question 7

*Rough translation*:

How many calls for bidding do exist for the development of software by public
institutions or by Private-Public-Partnerships since 2013?

**Answer**:

- Answer provided as data table

> The information on the costs of the individual orders and the commissioned
> company were removed from the tables or made anonymous. This is necessary
> because this information is protected by the German Basic Law in accordance
> with the company and business secrets of the contracted consulting companies.
> As operating and Trade secrets are all facts relating to a company,
> circumstances and processes that are not obvious, but can only be attributed
> to and are accessible by a limited group of people and the non-proliferation
> of which the legal entity has a legitimate interest. (BVerfGE 115, 205/230 for
> protection of Article 12 of the Basic Law[GG]). This includes, among other
> things, the costs of consulting contracts, provided that industry experts can
> draw conclusions from the information provided.
>
> With regard to the information on this question provided by the intelligence
> authorities, the answer to this question contained information which is of
> particular concern to the public interest and therefore cannot even be
> answered in a classified form. The constitutionally guaranteed question and
> information law of the German Bundestag vis-à-vis the Federal Government is
> limited by interests worthy of protection, such as the public interest, which
> also enjoy constitutional law. Disclosure of the information requested risks
> revealing details of the specific methodology and highly sensitive specific
> capabilities of the intelligence service. As a result, both governmental and
> non-governmental actors could draw conclusions about specific approaches and
> capabilities of the intelligence service. This would impose serious
> restrictions on the acquisition of information, with the result that the legal
> mandate of the respective intelligence service - the collection and evaluation
> of information - would ultimately not be fulfilled from abroad or from within
> Germany, which are of significance for security policy. The production of
> foreign or domestically related information is required for the security of
> the Federal Republic of Germany. Information gaps also with regard to the
> security situation of the Federal Republic of Germany Germany is threatening.
> A VS-classification and deposit of the requested Information in the secret
> protection agency of the German Bundestag would be to their considerable
> explosiveness with regard to the importance for the fulfilment of tasks of the
> respective intelligence service. The inquired In formation describe the
> capabilities and working methods of the secret agencies in such detail that an
> announcement can also only be made to a limited audience. For if the
> information in need of protection were to become known, there would be no use
> through other instruments of information retrieval. Thus the information
> requested is so sensitive that it cannot be used for any other purpose. In
> this respect, as an exception the right of Members to ask questions in
> relation to the interest of secrecy of the Federal Government.

**Results of Analysis**:

- 158 call for biddings in 126 projects

* * *

# Question 8

*Rough translation*:

How many calls for bidding do exist for adaption of self-developed software by
public institutions or by Private-Public-Partnerships since 2013?

**Answer**:

- Answer provided as data table

> The information on the costs of the individual orders and the commissioned
> company were removed from the tables or made anonymous. This is necessary
> because this information is protected by the German Basic Law in accordance
> with the company and business secrets of the contracted consulting companies.
> As operating and Trade secrets are all facts relating to a company,
> circumstances and processes that are not obvious, but can only be attributed
> to and are accessible by a limited group of people and the non-proliferation
> of which the legal entity has a legitimate interest. (BVerfGE 115, 205/230 for
> protection of Article 12 of the Basic Law[GG]). This includes, among other
> things, the costs of consulting contracts, provided that industry experts can
> draw conclusions from the information provided.
>
> With regard to the information on this question provided by the intelligence
> authorities, the answer to this question contained information which is of
> particular concern to the public interest and therefore cannot even be
> answered in a classified form. The constitutionally guaranteed question and
> information law of the German Bundestag vis-à-vis the Federal Government is
> limited by interests worthy of protection, such as the public interest, which
> also enjoy constitutional law. Disclosure of the information requested risks
> revealing details of the specific methodology and highly sensitive specific
> capabilities of the intelligence service. As a result, both governmental and
> non-governmental actors could draw conclusions about specific approaches and
> capabilities of the intelligence service. This would impose serious
> restrictions on the acquisition of information, with the result that the legal
> mandate of the respective intelligence service - the collection and evaluation
> of information - would ultimately not be fulfilled from abroad or from within
> Germany, which are of significance for security policy. The production of
> foreign or domestically related information is required for the security of
> the Federal Republic of Germany. Information gaps also with regard to the
> security situation of the Federal Republic of Germany Germany is threatening.
> A VS-classification and deposit of the requested Information in the secret
> protection agency of the German Bundestag would be to their considerable
> explosiveness with regard to the importance for the fulfilment of tasks of the
> respective intelligence service. The inquired In formation describe the
> capabilities and working methods of the secret agencies in such detail that an
> announcement can also only be made to a limited audience. For if the
> information in need of protection were to become known, there would be no use
> through other instruments of information retrieval. Thus the information
> requested is so sensitive that it cannot be used for any other purpose. In
> this respect, as an exception the right of Members to ask questions in
> relation to the interest of secrecy of the Federal Government.

**Results of Analysis**:

- 91 call for biddings in 61 projects

* * *

# Question 9

*Rough translation*:

Which calls for bidding had a binding requirement of Free licensed software? If
there were none, why not?

**Answer**:

- Answer provided as data table

> The information on the costs of the individual orders and the commissioned
> company were removed from the tables or made anonymous. This is necessary
> because this information is protected by the German Basic Law in accordance
> with the company and business secrets of the contracted consulting companies.
> As operating and Trade secrets are all facts relating to a company,
> circumstances and processes that are not obvious, but can only be attributed
> to and are accessible by a limited group of people and the non-proliferation
> of which the legal entity has a legitimate interest. (BVerfGE 115, 205/230 for
> protection of Article 12 of the Basic Law[GG]). This includes, among other
> things, the costs of consulting contracts, provided that industry experts can
> draw conclusions from the information provided.
>
> With regard to the information on this question provided by the intelligence
> authorities, the answer to this question contained information which is of
> particular concern to the public interest and therefore cannot even be
> answered in a classified form. The constitutionally guaranteed question and
> information law of the German Bundestag vis-à-vis the Federal Government is
> limited by interests worthy of protection, such as the public interest, which
> also enjoy constitutional law. Disclosure of the information requested risks
> revealing details of the specific methodology and highly sensitive specific
> capabilities of the intelligence service. As a result, both governmental and
> non-governmental actors could draw conclusions about specific approaches and
> capabilities of the intelligence service. This would impose serious
> restrictions on the acquisition of information, with the result that the legal
> mandate of the respective intelligence service - the collection and evaluation
> of information - would ultimately not be fulfilled from abroad or from within
> Germany, which are of significance for security policy. The production of
> foreign or domestically related information is required for the security of
> the Federal Republic of Germany. Information gaps also with regard to the
> security situation of the Federal Republic of Germany Germany is threatening.
> A VS-classification and deposit of the requested Information in the secret
> protection agency of the German Bundestag would be to their considerable
> explosiveness with regard to the importance for the fulfilment of tasks of the
> respective intelligence service. The inquired In formation describe the
> capabilities and working methods of the secret agencies in such detail that an
> announcement can also only be made to a limited audience. For if the
> information in need of protection were to become known, there would be no use
> through other instruments of information retrieval. Thus the information
> requested is so sensitive that it cannot be used for any other purpose. In
> this respect, as an exception the right of Members to ask questions in
> relation to the interest of secrecy of the Federal Government.

**Results of Analysis**:

- 30 call for biddings required the software to be Opensource[sic], 62 did not
  require the software to be Opensource[sic]. The data did not specify an
  answere in 66 cases.

![Opensource as requirement in call for biddings](../plots/question-9_requirement.png)
\ 

* * *

# Question 10

*Rough translation*:

How many software developments did public institutions or service providers in
their name develop since 2013, that were not assigned through a call for
bidding? Why was there no call for bidding? If the software was not released as
Free software, why not?

**Answer**:

- WIP
- Answer provided as data table
- There were 141 in-house software developments

> The information on the costs of the individual orders and the commissioned
> company were removed from the tables or made anonymous. This is necessary
> because this information is protected by the German Basic Law in accordance
> with the company and business secrets of the contracted consulting companies.
> As operating and Trade secrets are all facts relating to a company,
> circumstances and processes that are not obvious, but can only be attributed
> to and are accessible by a limited group of people and the non-proliferation
> of which the legal entity has a legitimate interest. (BVerfGE 115, 205/230 for
> protection of Article 12 of the Basic Law[GG]). This includes, among other
> things, the costs of consulting contracts, provided that industry experts can
> draw conclusions from the information provided.
>
> With regard to the information on this question provided by the intelligence
> authorities, the answer to this question contained information which is of
> particular concern to the public interest and therefore cannot even be
> answered in a classified form. The constitutionally guaranteed question and
> information law of the German Bundestag vis-à-vis the Federal Government is
> limited by interests worthy of protection, such as the public interest, which
> also enjoy constitutional law. Disclosure of the information requested risks
> revealing details of the specific methodology and highly sensitive specific
> capabilities of the intelligence service. As a result, both governmental and
> non-governmental actors could draw conclusions about specific approaches and
> capabilities of the intelligence service. This would impose serious
> restrictions on the acquisition of information, with the result that the legal
> mandate of the respective intelligence service - the collection and evaluation
> of information - would ultimately not be fulfilled from abroad or from within
> Germany, which are of significance for security policy. The production of
> foreign or domestically related information is required for the security of
> the Federal Republic of Germany. Information gaps also with regard to the
> security situation of the Federal Republic of Germany Germany is threatening.
> A VS-classification and deposit of the requested Information in the secret
> protection agency of the German Bundestag would be to their considerable
> explosiveness with regard to the importance for the fulfilment of tasks of the
> respective intelligence service. The inquired In formation describe the
> capabilities and working methods of the secret agencies in such detail that an
> announcement can also only be made to a limited audience. For if the
> information in need of protection were to become known, there would be no use
> through other instruments of information retrieval. Thus the information
> requested is so sensitive that it cannot be used for any other purpose. In
> this respect, as an exception the right of Members to ask questions in
> relation to the interest of secrecy of the Federal Government.

**Results of Analysis**:

- Administration claim to develop 141 software pieces, yet they only provide
  data for 140.
- For 14 projects the given reason for developing "in-house" was for economic
  reasons.
- 14 projects were developed "in-house" for "flexibility reasons".
- 14 projects were followup-projects for already existing "in-house"
  developments. There was jo reason specified, why the preceding project was
  developed "in-house".
- 21 projects were developed "in-house" in order to strengthen the own
  IT-Infrastructure and keep/develop knowledge about the product within the
  administration(s)
- 105 projects were developed "in-house", because the administration did not
  find a viable solution by third parties, neither proprietary, nor free.
- Some observations may be in multiple categories (nobs = 140, values = 168)

![Opensource as requirement in call for biddings](../plots/question-10_callforbidding.png)
\ 

* * *

# Question 11

*Rough translation*:

Which software has been developed by public institutions or their service
providers and used since 2013?

**Answer**:

- Answer provided as data table
- As the question was related to the names of the individual projects, the data
  itself is sufficient. Based on this, a normative analysis can be done.

> The information on the costs of the individual orders and the commissioned
> company were removed from the tables or made anonymous. This is necessary
> because this information is protected by the German Basic Law in accordance
> with the company and business secrets of the contracted consulting companies.
> As operating and Trade secrets are all facts relating to a company,
> circumstances and processes that are not obvious, but can only be attributed
> to and are accessible by a limited group of people and the non-proliferation
> of which the legal entity has a legitimate interest. (BVerfGE 115, 205/230 for
> protection of Article 12 of the Basic Law[GG]). This includes, among other
> things, the costs of consulting contracts, provided that industry experts can
> draw conclusions from the information provided.
>
> With regard to the information on this question provided by the intelligence
> authorities, the answer to this question contained information which is of
> particular concern to the public interest and therefore cannot even be
> answered in a classified form. The constitutionally guaranteed question and
> information law of the German Bundestag vis-à-vis the Federal Government is
> limited by interests worthy of protection, such as the public interest, which
> also enjoy constitutional law. Disclosure of the information requested risks
> revealing details of the specific methodology and highly sensitive specific
> capabilities of the intelligence service. As a result, both governmental and
> non-governmental actors could draw conclusions about specific approaches and
> capabilities of the intelligence service. This would impose serious
> restrictions on the acquisition of information, with the result that the legal
> mandate of the respective intelligence service - the collection and evaluation
> of information - would ultimately not be fulfilled from abroad or from within
> Germany, which are of significance for security policy. The production of
> foreign or domestically related information is required for the security of
> the Federal Republic of Germany. Information gaps also with regard to the
> security situation of the Federal Republic of Germany Germany is threatening.
> A VS-classification and deposit of the requested Information in the secret
> protection agency of the German Bundestag would be to their considerable
> explosiveness with regard to the importance for the fulfilment of tasks of the
> respective intelligence service. The inquired In formation describe the
> capabilities and working methods of the secret agencies in such detail that an
> announcement can also only be made to a limited audience. For if the
> information in need of protection were to become known, there would be no use
> through other instruments of information retrieval. Thus the information
> requested is so sensitive that it cannot be used for any other purpose. In
> this respect, as an exception the right of Members to ask questions in
> relation to the interest of secrecy of the Federal Government.

* * *

# Question 11a

*Rough translation*:

Which free software is being used by public administrations?

**Answer**:

- No data is provided in this case

> In view of the fact that (highly) critical vulnerabilities are repeatedly
> published for some of the listed products, the table was classified as
> "PC-only for official use". When the overview table becomes known or
> (unintentionally) "published", conclusions could be drawn about a large number
> of attack vectors for targeted attacks on weak points in the ICT
> infrastructure in the respective authorities.

**Notes**:

- This question can not be answered, as no data has been provided.

* * *

# Question 11b

*Rough translation*:

How many computers in the public administration have the needed applications for
encrypting E-Mail installed?

**Answer**:

- No data is provided in this case

> In view of the fact that (highly) critical vulnerabilities are repeatedly
> published for some of the listed products, the table was classified as
> "PC-only for official use". When the overview table becomes known or
> (unintentionally) "published", conclusions could be drawn about a large number
> of attack vectors for targeted attacks on weak points in the ICT
> infrastructure in the respective authorities.
>
> With regard to the information on this issue from the authorities, the answer
> to this question contained information that could be used to affect the
> well-being of the State to a particularly high degree and are therefore
> themselves classified and cannot be answered in this form. The
> constitutionally guaranteed Question and information law of the German
> Bundestag vis-à-vis the Federal Government is supported by equally
> constitutionally entitled defensible information. The danger is that details
> of the concrete methodology and of the highly sensitive specific intelligence
> capabilities would become known.
>
> With regard to the information on this question provided by the intelligence
> authorities, the answer to this question contained information which is of
> particular concern to the public interest and therefore cannot even be
> answered in a classified form. The constitutionally guaranteed question and
> information law of the German Bundestag vis-à-vis the Federal Government is
> limited by interests worthy of protection, such as the public interest, which
> also enjoy constitutional law. Disclosure of the information requested risks
> revealing details of the specific methodology and highly sensitive specific
> capabilities of the intelligence service. As a result, both governmental and
> non-governmental actors could draw conclusions about specific approaches and
> capabilities of the intelligence service. This would impose serious
> restrictions on the acquisition of information, with the result that the legal
> mandate of the respective intelligence service - the collection and evaluation
> of information - would ultimately not be fulfilled from abroad or from within
> Germany, which are of significance for security policy. The production of
> foreign or domestically related information is required for the security of
> the Federal Republic of Germany. Information gaps also with regard to the
> security situation of the Federal Republic of Germany Germany is threatening.
> A VS-classification and deposit of the requested Information in the secret
> protection agency of the German Bundestag would be to their considerable
> explosiveness with regard to the importance for the fulfilment of tasks of the
> respective intelligence service. The inquired In formation describe the
> capabilities and working methods of the secret agencies in such detail that an
> announcement can also only be made to a limited audience. For if the
> information in need of protection were to become known, there would be no use
> through other instruments of information retrieval. Thus the information
> requested is so sensitive that it cannot be used for any other purpose. In
> this respect, as an exception the right of Members to ask questions in
> relation to the interest of secrecy of the Federal Government.

**Notes**:

- This question can not be answered, as no data has been provided.

* * *

# Question 11c

*Rough translation*:

Which public keys for encrypted communication between citizens and authorities
do exist?

**Answer**:

- WIP

**Notes**:

- The data is unsuitable to answer the question.


