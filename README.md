# FOI Request Analysis 18/12906

> Analyze the answers ([18/12906](https://dipbt.bundestag.de/dip21/btd/18/129/1812906.pdf)) of Freedom of Information Request ([18/12471](https://dipbt.bundestag.de/doc/btd/18/124/1812471.pdf)) about usage of Free Software in German administrations

# ToDo

- WIP
- Write final Report
- Make repository [reuse](https://reuse.software/practices/2.0/) conform

# Maintainer

- Jan Weymeirsch - janwey AT fsfe DOT org

# Contributors

- Vincent - vincent AT fsfe DOT org

# Contribute

Any pull requests or suggestions are welcome at https://git.fsfe.org/janwey/ilfs-data or via e-mail to the maintainer.


